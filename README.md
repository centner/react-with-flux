# React learning project with:

- gulp task manager
- bootstrap/jquery
- browserify

# How to start?
From root project directory after cloning:

1. install node dependencies
```
sudo npm i
```
2. run gulp default task which serves css/js bundle
```
gulp
```
