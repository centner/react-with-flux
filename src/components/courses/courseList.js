"use strict";

var React = require('react');
var Link = require('react-router').Link;

var CourseList = React.createClass({
    propTypes: {
        courses: React.PropTypes.array.isRequired
    },

    render: function() {
        var createCourseRow = function(course) {
            return (
                <tr key={course.id}>
                    <td><Link to='manageCourse' params={{id: course.id}}>{course.id}</Link></td>
                    <td><Link to='manageCourse' params={{title: course.title}}>{course.title}</Link></td>
                    <td><Link to='manageCourse' params={{category: course.category}}>{course.category}</Link></td>
                    <td>{course.length}</td>
                </tr>
            );
        };

        return (
            <div>
                <table className='table'>
                    <thead>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Category</th>
                        <th>Length</th>
                    </thead>


                </table>
            </div>

        );
    }
});

module.exports = CourseList;
