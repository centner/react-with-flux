'use strict';

var React = require('react');
var Link = require('react-router').Link;
var CourseList = require('./CourseList');

var CoursePage = React.createClass({
    getInitialState: function() {
        return null;
    },

    render: function() {
        return (
            <div>
                <h1>Courses</h1>
                <Link to='addCourse' className='btn btn-default'>Add Course</Link>
                <CourseList />
            </div>
        );
    }

});

module.exports = CoursePage;
